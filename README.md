# **Resolução do Desafio DevOps**

Aqui estão os **desafios** da segunda fase do processo seletivo para os candidatos da ordem Jedi da **Let’s Code by Ada** para às vagas com o **time de DevOps**. 
 
# **Infraestrutura**
Selecionada a cloud AWS para realizar o desafio.

Selecionado o Gitlab como repositorio remoto e para a pipeline CI/CD.

**Cloud Resources:**

- S3 (tfstates)
- VPC
- EKS
- ELB Classic
- ECR
- RDS
- SSM

**Variaveis de ambiente no Gitlab**

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION (Opcional)
- AWS_SECRET_ACCESS_KEY
- TF_VAR_AWS_S3_TF_STATE_BUCKET
- TF_VAR_AWS_S3_TF_STATE_BUCKET_REGION
- TF_VAR_GITLAB_RUNNER_TOKEN

**Passos iniciais:**

- Criar um IAM User, gerar uma access key e salva-la como variavel de ambiente no gitlab.
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
- Criar um bucket S3 para armazenar o tfstate dos recursos e adicionar o nome e a regiao do bucket como variavel de ambiente do repositorio no Gitlab. 
  - O job 99-deploy-s3-backend na pipeline poderá gerar o bucket para você.
  - TF_VAR_AWS_S3_TF_STATE_BUCKET
  - TF_VAR_AWS_S3_TF_STATE_BUCKET_REGION
- Criar um Runner Token no Gitlab com a label "build" e salvar o seu token como variavel de ambiente no Gitlab. Este token sera usado no EKS.
  - TF_VAR_GITLAB_RUNNER_TOKEN
 
# **Pipeline**

A pipeline possui 4 stages:

- infrastructure
- build
- deploy
- after_deploy

A pipeline esta prepara para suportar a criação de multiambientes na mesma conta AWS baseado nas branches main e develop. Mais branches podem ser adicionadas com poucos ajustes no arquivo .gitlab-ci.yml e no arquivo terraform.tfvars da VPC.

### **Stage: infrastructure**

Iniciar o deploy da infraestrutura base na ordem indicada na stage:

- 01-deploy-vpc
- 02-deploy-eks
- 03-deploy-eks-addons
- 03-deploy-rds

___Obs: Os jobs 03-deploy-eks-addons e 03-deploy-rds podem ser iniciados simultaneamente.___

### **Stage: Build**

Após a infraestrutura base ter sido criada com sucesso você podera avancar para a proxima stage, a stage build.

A stage build ira iniciar o build das aplicações (frontend e backend) containerizadas.

Essa stage usará o Runner criado na infraestrutura base dentro do cluster EKS através da tag "build"

A execução com sucesso dessa stage irá iniciar automaticamente a proxima stage, a stage deploy.

### **Stage: Deploy**

Essa stage realiza o deploy das aplicacoes via helm usando as imagens geradas na stage anterior.

### **Stage: After Deploy**

Essa stage poderá ser executada após o deploy das aplicações para executar as queries no banco de dados afim de gerar os usuários para logar no sistema.

As queries estão no caminho IaC/database-migrations/users

![Login na aplicacao](https://willianromaocursos.blob.core.windows.net/public/Desafio%20Lets%20Code%20By%20Ada.png)

# **Destruição do ambiente**

A destruição do ambiente devera seguir a ordem abaixo da stage infrastructure:

- 11-destroy-eks-addons
- 11-destroy-rds
- 12-destroy-eks
- 13-destroy-vpc

___Obs: Os jobs 11-destroy-eks-addons e 11-destroy-rds podem ser iniciados simultaneamente.___

### **Destruições manuais:**

- Bucket S3
- IAM User
- Gitlab Runner Token
