export const environment = {
  production: true,
  apiURL: 'http://${ENV_API_URL}',
  clientId: 'my-angular-app',
  clientSecret: '@321',
  obterTokenUrl: '/oauth/token'
};
