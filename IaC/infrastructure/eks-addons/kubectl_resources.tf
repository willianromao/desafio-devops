resource "kubernetes_namespace" "backend" {
  metadata {
    name = "backend"
  }
}

resource "kubernetes_namespace" "frontend" {
  metadata {
    name = "frontend"
  }
}

resource "kubernetes_network_policy" "deny_frontend_private_subnets" {
  metadata {
    name      = "deny-frontend-private-subnets"
    namespace = "frontend"
  }

  spec {
    pod_selector {}

    egress {
      to {
        ip_block {
          cidr   = "0.0.0.0/0"
          except = data.terraform_remote_state.vpc.outputs.vpc.private_subnets_cidr_blocks
        }
      }
      to {
        namespace_selector {
          match_labels = {
            name = "frontend"
          }
        }
      }
    }

    policy_types = ["Egress"]
  }
}

data "kubernetes_resource" "ingress_external_ip" {
  depends_on = [ helm_release.ingress_nginx ]
  api_version = "v1"
  kind        = "Service"

  metadata {
    name      = "ingress-nginx-controller"
    namespace = "ingress-nginx"
  }
}

resource "aws_ssm_parameter" "ingress_external_ip" {
  name        = "/${local.default_name}/ingress_external_ip"
  description = "External URL"
  type        = "String"
  value       = data.kubernetes_resource.ingress_external_ip.object.status.loadBalancer.ingress[0].hostname
}

output "ingress_external_endpoint" {
  value = {
    hostname = data.kubernetes_resource.ingress_external_ip.object.status.loadBalancer.ingress[0].hostname
  }
}