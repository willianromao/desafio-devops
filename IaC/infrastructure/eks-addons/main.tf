terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.10.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
  backend "s3" {}
}

provider "helm" {
  kubernetes {
    config_path = "/tmp/config"
  }
}

provider "kubernetes" {
  config_path = "/tmp/config"
}