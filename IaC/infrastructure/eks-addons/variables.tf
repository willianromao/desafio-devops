variable "global_settings" {
  description = "Variaveis globais"
}

variable "branch" {
  description = "Cria a infraestrutura baseado na branch"
  default     = "main"
}

variable "AWS_S3_TF_STATE_BUCKET" {
  description = "Bucket ID de armazenamento dos tfstates"
  default     = "ada-letscode-rstate-bucket-52729"
}

variable "AWS_S3_TF_STATE_BUCKET_REGION" {
  description = "Regiao do bucket de armazenamento dos tfstates"
  default     = "us-east-1"
}

variable "GITLAB_RUNNER_TOKEN" {
  description = "Runner token para registrar o runner de build dos apps"
  sensitive   = true
}

locals {
  default_name = "${var.global_settings["${var.branch}"].organization}-${var.global_settings["${var.branch}"].project}-${var.branch}"
}