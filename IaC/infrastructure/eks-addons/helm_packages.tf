resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress-nginx"
  create_namespace = true

  set {
    name  = "controller.service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "controller.kind"
    value = "DaemonSet"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
    value = "true"
  }
}

resource "helm_release" "calico" {
  name             = "calico"
  repository       = "https://docs.tigera.io/calico/charts"
  chart            = "tigera-operator"
  namespace        = "tigera-operator"
  version          = "3.25.1"
  create_namespace = true

  set {
    name  = "installation.kubernetesProvider"
    value = "EKS"
  }
}

resource "helm_release" "gitlab_runner" {
  name             = "gitlab-runner"
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  namespace        = "gitlab-runner"
  create_namespace = true

  set {
    name  = "runnerToken"
    value = var.GITLAB_RUNNER_TOKEN
  }
  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com"
  }
  set {
    name  = "rbac.create"
    value = "true"
  }
  set {
    name  = "rbac.clusterWideAccess"
    value = "true"
  }
  set {
    name  = "runners.config"
    value = <<EOF
[[runners]]
  [runners.kubernetes]
    namespace = "{{.Release.Namespace}}"
    image = "ubuntu:20.04"
    privileged = true
  [[runners.kubernetes.volumes.empty_dir]]
    name = "docker-certs"
    mount_path = "/certs/client"
    medium = "Memory"
EOF
  }
}