module "ecr" {
  source = "terraform-aws-modules/ecr/aws"

  repository_name                   = "${local.default_name}-ecr"
  repository_read_write_access_arns = [module.eks.cluster_iam_role_arn]
  repository_force_delete           = true
  repository_lifecycle_policy = jsonencode({
    rules = [
      {
        rulePriority = 1,
        description  = "Keep last 10 images",
        selection = {
          tagStatus     = "tagged",
          tagPrefixList = ["letscodebyada-app"],
          countType     = "imageCountMoreThan",
          countNumber   = 10
        },
        action = {
          type = "expire"
        }
      }
    ]
  })

  tags = var.global_settings["${var.branch}"].tags
}

resource "aws_ssm_parameter" "repository_url" {
  name        = "/${local.default_name}/repository_url"
  description = "ECR Repository URL"
  type        = "String"
  value       = module.ecr.repository_url
}

output "ecr" {
  value = {
    repository_url          = module.ecr.repository_url
    repository_registry_id  = module.ecr.repository_registry_id
    repository_arn          = module.ecr.repository_arn
    repository_url_ssm_arn  = aws_ssm_parameter.repository_url.arn
    repository_url_ssm_name = aws_ssm_parameter.repository_url.name
  }
}