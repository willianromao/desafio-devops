terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.10.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.10.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
  backend "s3" {}
}

provider "aws" {
  region = var.global_settings["${var.branch}"].region
}

provider "helm" {
  kubernetes {
    config_path = "/tmp/config"
  }
}

provider "kubernetes" {
  config_path = "/tmp/config"
}