data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.AWS_S3_TF_STATE_BUCKET
    region = var.AWS_S3_TF_STATE_BUCKET_REGION
    key    = "IaC/infrastructure/vpc/${var.branch}/terraform.tfstate"
  }
}