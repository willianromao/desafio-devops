module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.0"

  cluster_name                   = "${local.default_name}-eks"
  cluster_version                = "1.27"
  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id = data.terraform_remote_state.vpc.outputs.vpc.vpc_id

  subnet_ids = [
    data.terraform_remote_state.vpc.outputs.vpc.public_subnets_ids[0],
    data.terraform_remote_state.vpc.outputs.vpc.public_subnets_ids[1]
  ]
  control_plane_subnet_ids = [
    data.terraform_remote_state.vpc.outputs.vpc.private_subnets_ids[0],
    data.terraform_remote_state.vpc.outputs.vpc.private_subnets_ids[1]
  ]

  eks_managed_node_groups = {
    group1 = {
      min_size     = 1
      max_size     = 3
      desired_size = 2

      instance_types = ["t3.medium"]
      capacity_type  = "ON_DEMAND"
    }
  }

  cluster_security_group_additional_rules = ({
    ingress_nodes_80 = {
      from_port                  = 80
      to_port                    = 80
      protocol                   = "tcp"
      type                       = "ingress"
      source_node_security_group = false
      cidr_blocks                = ["${data.terraform_remote_state.vpc.outputs.vpc.vpc_cidr_block}"]
    }
  })

  node_security_group_additional_rules = ({
    ingress_nodes_80 = {
      from_port                     = 80
      to_port                       = 80
      protocol                      = "tcp"
      type                          = "ingress"
      source_cluster_security_group = false
      cidr_blocks                   = ["${data.terraform_remote_state.vpc.outputs.vpc.vpc_cidr_block}"]
    }
  })

  tags = var.global_settings["${var.branch}"].tags
}

resource "aws_ssm_parameter" "cluster_name" {
  name        = "/${local.default_name}/cluster_name"
  description = "EKS Cluster Name"
  type        = "String"
  value       = module.eks.cluster_name
}

output "eks" {
  value = {
    cluster_name              = module.eks.cluster_name
    cluster_arn               = module.eks.cluster_arn
    cluster_endpoint          = module.eks.cluster_endpoint
    cluster_version           = module.eks.cluster_version
    cluster_security_group_id = module.eks.cluster_security_group_id
    node_security_group_id    = module.eks.node_security_group_id
    cluster_iam_role_arn      = module.eks.cluster_iam_role_arn
  }
}