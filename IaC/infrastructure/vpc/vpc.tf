module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.1"

  name                          = "${local.default_name}-vpc"
  cidr                          = var.global_settings["${var.branch}"].vpc.cidr
  azs                           = var.global_settings["${var.branch}"].vpc.zones
  private_subnets               = var.global_settings["${var.branch}"].vpc.private_subnets
  public_subnets                = var.global_settings["${var.branch}"].vpc.public_subnets
  enable_nat_gateway            = var.global_settings["${var.branch}"].vpc.nat_gateway
  manage_default_security_group = false
  map_public_ip_on_launch       = true

  tags = var.global_settings["${var.branch}"].tags
}

output "vpc" {
  value = {
    vpc_cidr_block              = module.vpc.vpc_cidr_block
    vpc_id                      = module.vpc.vpc_id
    zones                       = module.vpc.azs
    igw_id                      = module.vpc.igw_id
    natgw_ids                   = module.vpc.natgw_ids
    private_subnets_ids         = module.vpc.private_subnets
    public_subnets_ids          = module.vpc.public_subnets
    private_subnets_cidr_blocks = module.vpc.private_subnets_cidr_blocks
    public_subnets_cidr_blocks  = module.vpc.public_subnets_cidr_blocks
  }
}