terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.10.0"
    }
  }
  backend "s3" {}
}

provider "aws" {
  region = var.global_settings["${var.branch}"].region
}