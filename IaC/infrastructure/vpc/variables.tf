variable "global_settings" {
  description = "Variaveis globais"
}

variable "branch" {
  description = "Cria a infraestrutura baseado na branch"
  default     = "main"
}

locals {
  default_name = "${var.global_settings["${var.branch}"].organization}-${var.global_settings["${var.branch}"].project}-${var.branch}"
}