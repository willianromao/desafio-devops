global_settings = {
  main = {
    region       = "us-east-1"
    organization = "ada"
    project      = "letscode"
    env          = "main"
    tags = {
      Organization = "Ada.tech"
      Project      = "Lets Code"
      ManagedBy    = "Terraform"
      Environment  = "main"
    }
    vpc = {
      cidr            = "10.0.0.0/16"
      zones           = ["us-east-1a", "us-east-1b", "us-east-1c"]
      private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
      public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
      nat_gateway     = true

    }
  }
  develop = {
    region       = "us-east-1"
    organization = "ada"
    project      = "letscode"
    env          = "develop"
    tags = {
      Organization = "Ada.tech"
      Project      = "Lets Code"
      ManagedBy    = "Terraform"
      Environment  = "develop"
    }
    vpc = {
      cidr            = "11.0.0.0/16"
      zones           = ["us-east-1a", "us-east-1b", "us-east-1c"]
      private_subnets = ["11.0.1.0/24", "11.0.2.0/24", "11.0.3.0/24"]
      public_subnets  = ["11.0.101.0/24", "11.0.102.0/24", "11.0.103.0/24"]
      nat_gateway     = true

    }
  }
}