resource "kubernetes_secret" "db_login_secret" {
  metadata {
      name      = "${local.default_name}db-secret"
      namespace = "backend"
  }
  data = {
      MYSQL_DB_USER = "${local.default_name}user"
      MYSQL_DB_PASS = random_id.password.hex
      MYSQL_DB_HOST = "jdbc:mysql://${module.db.db_instance_endpoint}/${local.default_name}db"
  }
  type = "Opaque"
}

output "db_login_secret_name" {
  value = {
    name = "${local.default_name}db-secret"
    namespace = "backend"
    }
}