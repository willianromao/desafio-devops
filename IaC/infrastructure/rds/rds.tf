resource "random_id" "password" {
  byte_length = 16
}

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier                  = "${local.default_name}instance"
  engine                      = "mysql"
  engine_version              = "5.7"
  instance_class              = "db.t2.micro"
  allocated_storage           = 5
  db_name                     = "${local.default_name}db"
  username                    = "${local.default_name}user"
  password                    = random_id.password.hex
  manage_master_user_password = false
  port                        = "3306"
  vpc_security_group_ids      = [module.sg_db.security_group_id]
  monitoring_interval         = "30"
  monitoring_role_name        = "MyRDSMonitoringRole"
  create_monitoring_role      = true
  tags                        = var.global_settings["${var.branch}"].tags
  create_db_subnet_group      = true
  subnet_ids                  = data.terraform_remote_state.vpc.outputs.vpc.private_subnets_ids
  family                      = "mysql5.7"
  major_engine_version        = "5.7"
  deletion_protection         = false
  storage_encrypted           = false
  skip_final_snapshot         = true
}

output "db_cluster" {
  value = {
    db_instance_address           = module.db.db_instance_address
    db_instance_endpoint          = module.db.db_instance_endpoint
    db_instance_arn               = module.db.db_instance_arn
    db_instance_availability_zone = module.db.db_instance_availability_zone
    db_instance_engine            = module.db.db_instance_engine
    db_instance_name              = module.db.db_instance_name
    db_name                       = "${local.default_name}db"
  }
}

output "db_login" {
  value = {
    db_host     = "jdbc:mysql://${module.db.db_instance_endpoint}/${local.default_name}db"
    db_username = "${local.default_name}user"
    db_password = random_id.password.hex
  }
  sensitive = true
}