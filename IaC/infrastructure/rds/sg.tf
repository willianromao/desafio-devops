module "sg_db" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "${local.default_name}db-sg"
  description = "MySQL access"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.vpc_id
  egress_with_cidr_blocks = [
    {
      rule = "all-all"
    }
  ]
  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "mysql-tcp"
      source_security_group_id = data.terraform_remote_state.eks.outputs.eks.node_security_group_id
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 1

}

output "sg_db" {
  value = {
    security_group_id   = module.sg_db.security_group_id
    security_group_arn  = module.sg_db.security_group_arn
    security_group_name = module.sg_db.security_group_name
  }
}