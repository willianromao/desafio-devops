variable "branch" {
  description = "Cria a infraestrutura baseado na branch"
  default     = "main"
}

variable "AWS_S3_TF_STATE_BUCKET" {
  description = "Bucket ID de armazenamento dos tfstates"
  default     = "ada-letscode-rstate-bucket-52729"
}

variable "AWS_S3_TF_STATE_BUCKET_REGION" {
  description = "Regiao do bucket de armazenamento dos tfstates"
  default     = "us-east-1"
}

variable "schema_version" {}