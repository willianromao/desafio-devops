resource "null_resource" "users" {
  triggers = { onVersionChange = "${timestamp()}" }
  provisioner "local-exec" {
    command = "flyway repair; flyway migrate -locations=filesystem:./users -target=${var.schema_version}"
    environment = {
      FLYWAY_USER="${data.terraform_remote_state.rds.outputs.db_login.db_username}"
      FLYWAY_PASSWORD="${data.terraform_remote_state.rds.outputs.db_login.db_password}"
      FLYWAY_URL="${data.terraform_remote_state.rds.outputs.db_login.db_host}"
    }    
  }
}