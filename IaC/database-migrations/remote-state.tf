data "terraform_remote_state" "rds" {
  backend = "s3"
  config = {
    bucket = var.AWS_S3_TF_STATE_BUCKET
    region = var.AWS_S3_TF_STATE_BUCKET_REGION
    key    = "IaC/infrastructure/rds/${var.branch}/terraform.tfstate"
  }
}