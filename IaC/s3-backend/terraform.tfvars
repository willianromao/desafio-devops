global_settings = {
  region       = "us-east-1"
  organization = "ada"
  project      = "letscode"
  env          = "main"
  tags = {
    Organization = "Ada.tech"
    Project      = "Lets Code"
    ManagedBy    = "Terraform"
    Environment  = "main"
  }
}