resource "random_id" "sufix" {
  byte_length = 2
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${local.default_name}-rstate-bucket-${random_id.sufix.dec}"
  tags   = var.global_settings.tags

  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rm s3://${self.id}/ --recursive"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_ownership_controls" "bucket_ownership_controls" {
  bucket = aws_s3_bucket.bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"

  depends_on = [
    aws_s3_bucket_ownership_controls.bucket_ownership_controls
  ]
}

resource "aws_s3_bucket_public_access_block" "public_access_blocked" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

output "bucket" {
  value = {
    id  = aws_s3_bucket.bucket.id
    arn = aws_s3_bucket.bucket.arn
  }
}