resource "helm_release" "frontend" {
  name      = "${local.default_name}-fe"
  chart     = "./helmchart"
  namespace = "frontend"

  set {
    name  = "image.repository"
    value = "${data.terraform_remote_state.eks.outputs.ecr.repository_url}:${var.APP_NAME}-${var.APP_HASH}"
  }
  set {
    name  = "ingress.host"
    value = data.terraform_remote_state.eks_addons.outputs.ingress_external_endpoint.hostname
  }
}

output "frontend" {
  value = data.terraform_remote_state.eks_addons.outputs.ingress_external_endpoint.hostname
}