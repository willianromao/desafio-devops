terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.10.1"
    }
  }
  backend "s3" {}
}

provider "helm" {
  kubernetes {
    config_path = "/tmp/config"
  }
}