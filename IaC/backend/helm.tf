resource "helm_release" "backend" {
  name      = "${local.default_name}-be"
  chart     = "./helmchart"
  namespace = "backend"

  set {
    name  = "image.repository"
    value = "${data.terraform_remote_state.eks.outputs.ecr.repository_url}:${var.APP_NAME}-${var.APP_HASH}"
  }
  set {
    name  = "envFromSecret"
    value = data.terraform_remote_state.rds.outputs.db_login_secret_name.name
  }
  set {
    name  = "ingress.host"
    value = data.terraform_remote_state.eks_addons.outputs.ingress_external_endpoint.hostname
  }
}